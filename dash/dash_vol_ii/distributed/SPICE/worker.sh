#!/bin/bash -l

SCHEDULER=${1}
echo "worker: $(hostname) $(hostname -i) $$ $PPID <$*> <$#> <${SCHEDULER}>"

BASE=~/miniconda2/bin
${BASE}/dask-worker ${SCHEDULER} --nthreads 4
