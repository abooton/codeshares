import matplotlib.pyplot as plt

import pandas as pd

import datashader as ds
import datashader.transfer_functions as tf
from datashader.colors import Hot


time_period = 60


def bin_data():
    global time_period, grouped, group_count, counter, times, groups
    grouped = df.groupby([times.hour, times.minute // time_period])
    groups = sorted(grouped.groups.keys(), key=lambda r: (r[0], r[1]))
    group_count = len(groups)
    counter = 0


class LiveImageDisplay(object):
    def __init__(self, h=500, w=500, niter=50, radius=2., power=2):
        self.height = h
        self.width = w

    def __call__(self, xstart, xend, ystart, yend):
        canvas = ds.Canvas(plot_width=self.width,
                           plot_height=self.height,
                           x_range=(xstart, xend),
                           y_range=(ystart, yend))

        agg = canvas.points(df, 'dropoff_x', 'dropoff_y',
                            ds.count('trip_distance'))

        img = tf.interpolate(agg, cmap=Hot, how='log')

        print(img.data.shape, img.data.dtype)
        return img.data

    def ax_update(self, ax):
        ax.set_autoscale_on(False)  # Otherwise, infinite loop

        # Get the number of points from the number of pixels in the window
        dims = ax.axesPatch.get_window_extent().bounds
        self.width = int(dims[2] + 0.5)
        self.height = int(dims[2] + 0.5)

        # Get the range for the new area
        xstart, ystart, xdelta, ydelta = ax.viewLim.bounds
        xend = xstart + xdelta
        yend = ystart + ydelta

        # Update the image object with our new data and extent
        im = ax.images[-1]
        im.set_data(self.__call__(xstart, xend, ystart, yend))
        im.set_extent((xstart, xend, ystart, yend))
        ax.figure.canvas.draw_idle()

path = './nyc_taxi.csv'
datetime_field = 'tpep_dropoff_datetime'
cols = ['dropoff_x', 'dropoff_y', 'trip_distance', datetime_field]

df = pd.read_csv(path, usecols=cols, parse_dates=[datetime_field]).dropna(axis=0)
times = pd.DatetimeIndex(df[datetime_field])
group_count = grouped = groups = None
bin_data()


xmin = -8240227.037
ymin = 4974203.152
xmax = -8231283.905
ymax = 4979238.441

img = LiveImageDisplay()
Z = img(xmin, xmax, ymin, ymax)

fig1, ax2 = plt.subplots(1, 1)
ax2.imshow(Z, origin='lower', extent=(xmin, xmax, ymin, ymax))

# Connect for changing the view limits
ax2.callbacks.connect('xlim_changed', img.ax_update)
ax2.callbacks.connect('ylim_changed', img.ax_update)

plt.show()
