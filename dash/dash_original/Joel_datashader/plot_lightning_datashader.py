import datashader as ds
import pickle
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib as mpl
import numpy as np

file = open('LDat201607241200Z', 'r')
reg_data = pickle.load(file)
reg_dat = {'LNGD': reg_data['LNGD'], 'LTTD': reg_data['LTTD']}
df = pd.DataFrame(reg_dat)

cmap = cm.get_cmap('jet', 5)
norm = mpl.colors.Normalize(clip=False, vmin=1, vmax=200)

class LiveImageDisplay(object):
    def __init__(self, h=500, w=500, niter=50, radius=2., power=2):
        self.height = h
        self.width = w

    def __call__(self, xstart, xend, ystart, yend):
        canvas = ds.Canvas(plot_width=self.width,
                           plot_height=self.height,
                           x_range=(xstart, xend),
                           y_range=(ystart, yend))

        agg = canvas.points(df, 'LNGD', 'LTTD')

        self.data = np.ma.masked_equal(agg.data, 0)
        print np.max(self.data), np.mean(self.data)
        print self.data.shape
        return self.data

    def ax1_update(self, ax):

        ax.set_autoscale_on(False)  # Otherwise, infinite loop

        # Get the number of points from the number of pixels in the window
        dims = ax.axesPatch.get_window_extent().bounds
        self.width = int(dims[2] + 0.5)
        self.height = int(dims[2] + 0.5)

        print dims
        # Get the range for the new area
        xstart, ystart, xdelta, ydelta = ax.viewLim.bounds
        xend = xstart + xdelta
        yend = ystart + ydelta

        print xstart, xend, ystart, yend

        # Update the image object with our new data and extent
        im = ax.images[-1]
        data = self.__call__(xstart, xend, ystart, yend)
        im.set_data(data)
        im.set_extent((xstart, xend, ystart, yend))
        self.cbar.set_clim(vmin=np.nanmin(data), vmax=200)
        self.cbar.draw_all()
        cmap.set_under('w')

        ax.set_title('Total = {}, Max = {}, Mean = {}'.format(
            np.sum(self.data), np.max(self.data), np.mean(self.data)))
        ax.figure.canvas.draw_idle()

    def ax2_update(self, ax):

        ax.set_autoscale_on(False)
        self.hist_ax.set_autoscale_on(False)
        self.hist_ax.clear()
        make_bar_plot(self.hist_ax, self.data)


xmin = -180
ymin = -90
xmax = 180
ymax = 90

img = LiveImageDisplay()
Z = img(xmin, xmax, ymin, ymax)

ax1 = plt.subplot2grid((10, 9), (0, 0), colspan=8, rowspan=5)
ax2 = plt.subplot2grid((10, 9), (5, 0), colspan=8, rowspan=5)
ax3 = plt.subplot2grid((10, 9), (0, 8), rowspan=10)
axim = ax1.imshow(Z, origin='lower', vmin=1, vmax=200,
                  extent=(xmin, xmax, ymin, ymax))

setattr(img, 'hist_ax', ax2)

ax1.set_title('Total = {}, Max = {}, Mean = {}'.format(np.sum(Z),
                                                       np.max(Z),
                                                       np.mean(Z)))

# Connect for changing the view limits
ax1.callbacks.connect('xlim_changed', img.ax1_update)
ax1.callbacks.connect('ylim_changed', img.ax1_update)

def make_bar_plot(ax, data):

    flattened = data.flatten()
    heights = np.bincount(flattened)
    lefts = np.array(range(*heights.shape))
    bars = ax.bar(lefts-0.5, heights, width=1, lw=0,
                  color=np.array([cmap(i) for i in lefts]))
    return bars

bars = make_bar_plot(ax2, Z)

cbar = mpl.colorbar.ColorbarBase(ax3, orientation='vertical', norm=norm,
                                 cmap=cmap, extend='neither',
                                 extendfrac='auto')

cmap.set_under('w')

setattr(img, 'cbar', cbar)

ax1.callbacks.connect('ylim_changed', img.ax2_update)

# ax1.coastlines()
# ax1.stock_img()

plt.show()
