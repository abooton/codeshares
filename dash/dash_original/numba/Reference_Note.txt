Created Iris branch : 
https://github.com/pp-mo/iris/pull/25
"Numba speedup for PointInCell."

(my repo)
 * (1) Replaced complex code with much simpler nested Python loops.
 * (2) Used numba to compile it
 * (3) evaluated with task from Iris integration test (??)
 * (4) --> about 2* faster than the existing complex sparse-matrix implementation
 

