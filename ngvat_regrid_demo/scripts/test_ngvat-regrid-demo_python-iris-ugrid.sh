#!/usr/bin/bash
# Test the conda environment provided for trial of regridding code

# Fail on any errors.
set -e

# location of the working script files (including this)
SCRIPTDIR=$(dirname $0)  # path containing this script
SCRIPTDIR=$(readlink -f $SCRIPTDIR)  # --> absolute path

# location of the env to be tested
DEMO_ENV_NAME=ngvat_regrid_demo_env
export DEMO_ENV_PATH=/project/avd/ng-vat/environments/$DEMO_ENV_NAME

# Run the test in the target env
$DEMO_ENV_PATH/bin/python $SCRIPTDIR/test_ngvat-regrid-demo_python-iris-ugrid.py
