#!/usr/bin/bash
#
# Build a conda environment for trials of the NG-VAT regridding code.
# For the NGVAT-0.1 delivery goal : see https://jira:6391/browse/AVD-1485
#
# Usage: just run this script.
# Destroy any existing test-env and create a new one, at the standard location.
#
# Requires:
#   write access to /project/avd/ng-vat/environments
#   write access to any existing env dir
#   write access to $TMPDIR for temporary working files
#   conda
#   git
#

# Fail on any errors.
set -e


#=======================================
# Settings and references
#

# location of the working script files (including this)
SCRIPTDIR=$(dirname $0)  # path containing this script
SCRIPTDIR=$(readlink -f $SCRIPTDIR)  # --> absolute path

# location of the env to be produced
DEMO_ENV_NAME=ngvat_regrid_demo_env
DEMO_ENV_PATH=/project/avd/ng-vat/environments/$DEMO_ENV_NAME

# Working directory path
WORKDIR=$TMPDIR/build_ngvat_regrid_env


#=======================================
# Preliminary operation (needs a workdir first)
#

# ensure workdir exists + empty
echo ""
echo ""
echo "-------------------------"
echo "MAKE NG-VAT regrid env : destroy + recreate working directory '$WORKDIR'"
echo "  working in : $WORKDIR  ..."
echo ""
rm -rf $WORKDIR
mkdir -p $WORKDIR


#=======================================
# More setup work : make some reference files, should be quick
#

# Iris repo + checkout tag
# FOR NOW = latest "ugrid" feature-branch code
IRIS_COMMIT="1938e144dd783c47ca523dddb1d5f78477603f06"
IRIS_URL="https://github.com/SciTools/iris/archive/$IRIS_COMMIT.zip"

# Iris dependencies calculation
# standard "groups"
IRIS_REQTS_GROUPS="test"
# additional (optional) dependencies
IRIS_REQTS_ADDITIONAL_FILE=$WORKDIR/iris_reqts_additional_packages.txt
cat <<EOF >$IRIS_REQTS_ADDITIONAL_FILE
mo_pack
nc-time-axis
EOF

# Iris-ugrid repo + checkout tag
# FOR NOW = latest "master" code
IRISUGRID_COMMIT=5ec19d8a0ed1d53b9abe3f89c99991e0ed68cd2b
IRISUGRID_URL="https://github.com/SciTools-incubator/iris-ugrid/archive/$IRISUGRID_COMMIT.zip"

# Iris-ugrid dependencies
IRISUGRID_REQTS_ADDITIONAL_FILE=$WORKDIR/irisugrid_additional_packages.txt
cat <<EOF >$IRISUGRID_REQTS_ADDITIONAL_FILE
esmpy
EOF

# Additional packages wanted in the demo environment
EXTRA_PACKAGES_FILE=$WORKDIR/env_additional_package_reqts.txt
cat <<EOF >$EXTRA_PACKAGES_FILE
# ipython
# ipywidgets
# jupyterlab
mo_pack
# mo_notebook
# mo_sitecustomize
EOF

# Conda channels settings
CONDA_CHANNELS_OPTS="
--override-channels
--channel https://conda.anaconda.org/conda-forge/linux-64
--channel https://conda.anaconda.org/conda-forge/noarch
--channel https://conda.anaconda.org/anaconda/linux-64
--channel https://conda.anaconda.org/anaconda/noarch
"

# Use conda "base" as the default python
# (currently only used for iris requirements generation)
CONDADIR=$(dirname $(which conda))
# current miniconda install structure has conda executable in a subdir ?
CONDA_BINDIR=$(dirname $CONDADIR)/bin
source $CONDA_BINDIR/activate


#=======================================
# Actions
#

#
# (1) Ensure env dir can be created / is empty
#
echo ""
echo ""
echo "-------------------------"
echo "MAKE NG-VAT regrid env : destroy and recreate target env directory '$DEMO_ENV_PATH'"
echo "  working in : $DEMO_ENV_PATH  (N.B. this bit can be slow!) ..."
echo ""
rm -rf $DEMO_ENV_PATH
mkdir -p $DEMO_ENV_PATH

#
# (2) Start from a checkout of iris, and  capture iris "standard requirements".
# Because : we don't want to install iris from a channel, as that limits us to
#   an actual release.

DEMO_CONDA_REQUIREMENTS_FILE=$WORKDIR/ngvat_0.1_conda_requirements.txt

echo ""
echo ""
echo "-------------------------"
echo "MAKE NG-VAT regrid env : get iris + run gen_conda_requirements ..."
echo ""

cd $WORKDIR

# Fetch iris @IRIS_COMMIT from master repo.
wget -O iris.zip $IRIS_URL
unzip -q iris.zip
mv iris-$IRIS_COMMIT iris
IRIS_REPO=$WORKDIR/iris

# Get iris standard requirements
python $IRIS_REPO/requirements/gen_conda_requirements.py --groups $IRIS_REQTS_GROUPS >$DEMO_CONDA_REQUIREMENTS_FILE

#
# (2a) Add specific optional iris requirements we wanted to include
#
# (Note: grep allows to skip 'commented' lines)
cat $IRIS_REQTS_ADDITIONAL_FILE | grep -v "^ *#" >>$DEMO_CONDA_REQUIREMENTS_FILE

#
# (3) Add in iris-ugrid requirements (packages)
#
echo ""
echo ""
echo "-------------------------"
echo "MAKE NG-VAT regrid env : get iris-ugrid + find its requirements ..."
echo ""

# Fetch iris-ugrid @IRISUGRID_COMMIT from master repo.
wget -O iris_ugrid.zip $IRISUGRID_URL
unzip -q iris_ugrid.zip
mv iris-ugrid-$IRISUGRID_COMMIT iris_ugrid
IRISUGRID_REPO=$WORKDIR/iris_ugrid

IRISUGRID_REQTS_FILE=$IRISUGRID_REPO/requirements/conda_requirements.txt
cat $IRISUGRID_REQTS_FILE | grep -v "^ *#" >>$DEMO_CONDA_REQUIREMENTS_FILE

#
# (3a) Add in "optional"  additional iris-ugrid package requirements
#
cat $IRISUGRID_REQTS_ADDITIONAL_FILE | grep -v "^ *#" >>$DEMO_CONDA_REQUIREMENTS_FILE


#
# (4) Add in the additional "general" packages we want in the env
#
cat $EXTRA_PACKAGES_FILE | grep -v "^ *#" >>$DEMO_CONDA_REQUIREMENTS_FILE


#
# (5) build the env
#
echo ""
echo ""
echo "-------------------------"
echo "MAKE NG-VAT regrid env : build the conda env ..."
echo ""
rm -rf $DEMO_ENV_PATH;
conda create -p $DEMO_ENV_PATH $CONDA_CHANNELS_OPTS --file $DEMO_CONDA_REQUIREMENTS_FILE -y

#
# (6) activate the env
#
source activate $DEMO_ENV_PATH


#
# (7) install iris into the env
#
# NOTE: as pip install -e is not currently working, we must currently use
# setup.py directly.
#  - This will build std_names and pyke_rules automatically.
#  - We have already sorted all the dependencies.
echo ""
echo ""
echo "-------------------------"
echo "MAKE NG-VAT regrid env : install iris ..."
echo ""
cd $IRIS_REPO
python setup.py install --quiet
cd $WORKDIR


#
# (8) install iris-ugrid into the env
#
echo ""
echo ""
echo "-------------------------"
echo "MAKE NG-VAT regrid env : install iris-ugrid ..."
echo ""
cd $IRISUGRID_REPO

# we don't have an installer for this yet.
# so for now, just copy the code into a site-packages subdir.
PYTHON_VERSION=$(python -c "import sys; vmin,vmax=sys.version.split('.')[:2]; print(f'{vmin}.{vmax}')")
SITE_PACKAGES_DIR=$DEMO_ENV_PATH/lib/python${PYTHON_VERSION}/site-packages
cp -r iris_ugrid $SITE_PACKAGES_DIR/iris_ugrid

cd $WORKDIR


#
# (9) copy in the demo script
#
echo ""
echo ""
echo "-------------------------"
echo "MAKE NG-VAT regrid env : copy demo script ..."
echo ""
cp $SCRIPTDIR/regrid_xios_to_um.py $DEMO_ENV_PATH/bin/


#
# (10) copy in the SPICE runner script
#
echo ""
echo ""
echo "-------------------------"
echo "MAKE NG-VAT regrid env : copy spice-runner script ..."
echo ""
cp $SCRIPTDIR/spice_run_regrid.sh $DEMO_ENV_PATH/bin/


#
# (11) delete the temporary files
#
rm -rf $WORKDIR


echo ""
echo ""
echo "-------------------------"
echo "MAKE NG-VAT regrid env :"
echo ""
echo "COMPLETED OK."
echo "  result env at : $DEMO_ENV_PATH"
echo ""
