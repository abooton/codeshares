
"""
Create dummy data, add it to an iris cube and tweak the forecast reference time information.
"""

# See operational StaGE example for more examples of adding time coordinates to cubes.
# https://exxgitrepo:8443/projects/PP/repos/stage/browse/lib/stage/tests/unit_tests/plugins/core/test_ConvertTimeToSeconds.py

import numpy as np
import iris
from cf_units import Unit
import datetime

# Create dummy data
lon_step = 45
lons = np.arange(-180 + lon_step, +180 - lon_step, lon_step)
lat_step = 30
lats = np.arange(-90 + lat_step, +90 - lat_step, lat_step)
data = np.arange(lats.size*lons.size).reshape(lats.size,lons.size)


# Create a cube with longitude, latitude and forecast_reference_time
# Then update the forecast_reference_time.

cube = iris.cube.Cube(data, long_name='test_data')
# Add lats and lons
lon = iris.coords.DimCoord(lons, "longitude", units="degrees")
lat = iris.coords.DimCoord(lats, "latitude", units="degrees")
cube.add_dim_coord(lon, 1)
cube.add_dim_coord(lat, 0)
cube.coord("latitude").guess_bounds()
cube.coord("longitude").guess_bounds()

# Add forecast reference time
unit = Unit('hours since 1970-01-01 00:00:00', calendar='gregorian')
frt = unit.date2num(datetime.datetime(2015, 6, 2, 0, 0))  # frt = 398112.0
forecast_ref_time_coord = iris.coords.AuxCoord(frt,
                                               units=unit,
                                               standard_name='forecast_reference_time')
cube.add_aux_coord(forecast_ref_time_coord)

# Alter the forecast reference time to a different date
frt = frt + 6   # 2015-06-18 00:00:00
forecast_ref_time_coord = iris.coords.AuxCoord(frt,
                                               units=Unit('hours since 1970-01-01 00:00:00',
                                                          calendar='gregorian'),
                                               standard_name='forecast_reference_time')
cube.remove_coord('forecast_reference_time')
cube.add_aux_coord(forecast_ref_time_coord)

# Or tweak the value of the frt
cube.coords('forecast_reference_time')[0].points += 6

print('The cube...')
print(cube)
