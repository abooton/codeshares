'''
Script for parallelizng calls to a function using Python's multiprocessing.pool

Simply: 
    * set the number of separate Python processes
    * specify the function to be called
    * list the arguments
The results are collated in a list.

See "main" section at bottom for example use.


The script is a simplified version of that used in the operational StaGE suites, for processing
in which hundreds of diagnostics are processed in parallel.
Original script, written by Ben Fitzpatrick is here:
https://exxgitrepo:8443/projects/PP/repos/decoupler/browse/suites/bin/decoupler_run_stage_multiprocessing.py#121

It is called from rose suite app:  https://exxgitrepo:8443/projects/PP/repos/decoupler/browse/suites/standard_v1/app/run_stage/rose-app.conf


Created on May 27, 2019

@author: frab
'''

import multiprocessing
import sys
import time
import timeit


# Used example here for using the callback to return output from the run_stage function
# https://stackoverflow.com/questions/8533318/multiprocessing-pool-when-to-use-apply-apply-async-or-map


_RESULT_POLLING_SLEEP = 0.1


def process_id_result_ok(id_, result):
    """Process a particular result object labelled with id_.

    Parameters
    ----------
    id_ : string
        Label for the result, typically derived from a stash code.
    result : multiprocessing.AsyncResult
        Result object for the processing of this id_.

    Returns
    -------
    True if result could be obtained, False if there was an exception.

    """
    try:
        return_value = result.get()
        # print(id_, "result", return_value)
    except Exception as exc:
        sys.stderr.write("{}: ERROR: {}\n".format(id_, exc))
        sys.stderr.flush()
        return False
    return True


def pool_submission(func_to_execute, arglist_list, pool_size):
    """Launch the pool of workers for the function_to_execute for the items
    in each argv entry.

    Parameters
    ----------
    func_to_execute : function
        The function you wish to run!
    arglist_list : tuple of lists
        The arguments to pass to the function being run via multiprocessor.pool
        Each argument within the list should be a list, as each of these
        will be added to the pool of workers. 
    pool_size : integer
        Number of separate Python processes to have in the pool.
    """
    print('arglist_list',arglist_list)

    default_pool = multiprocessing.Pool(processes=pool_size)

    checks = {}
    result_list = []
    for procnum, args in enumerate(arglist_list):
        if not isinstance(args, list):
            args = [args]
        arglist = args    
        id_ = "{0}".format(procnum)
        checks[id_] = default_pool.apply_async(
            func_to_execute,
            args=(arglist),
            callback=result_list.append,  # record the output from the function in resutls_list variable
            )

    default_pool.close()

    return_code = 0
    while checks and not return_code:
        time.sleep(_RESULT_POLLING_SLEEP)
        sys.stdout.flush()
        for id_, check in list(checks.copy().items()):
            if not check.ready():
                continue
            if not process_id_result_ok(id_, check):
                sys.stderr.write("Terminating pool\n")                
                default_pool.terminate()
                return_code = 1
                break
            checks.pop(id_)
    default_pool.join()
    if return_code:
        sys.exit(return_code)

    return result_list


if __name__ == '__main__':
    
    # Testing Example:
    
    def my_test(var1, procnum):
        from os import getpid
        print('I am number {} in process {} - returning'.format(procnum, getpid()))
        output = 'var in was: {}'.format(var1)
        time.sleep(1)
        return output
    
    
    # Run in in loop (not parallelized)
    print('Traditional Loop...')
    func_argv = (['a', 1], ['b', 2], ['c', 3])
    results_list = []
    for val, procnum in func_argv:
        result = my_test(val, procnum=procnum)
        results_list.append(result)
    print('Looping:  results:', results_list)
    
    
    # Run test in parallel
    pool_size = 4
    func_to_execute = my_test
    func_argv = (['a', 1], ['b', 2], ['c', 3])
    results_list = []
    # Get the results back from the workers
    print('\nstart multiprocessing')
    start_time = timeit.default_timer()
    results_list = pool_submission(func_to_execute, func_argv, pool_size)
    print('Multiprocessing:  results:', results_list)
    print('Time taken:', timeit.default_timer() - start_time, 'secs' )


    print('Finished')
