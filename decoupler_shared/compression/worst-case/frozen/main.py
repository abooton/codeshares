from collections import Iterable
from glob import glob
import os

import numpy as np

from decoupler_shared.fields_diagnostic_size import _ff_scan as ff_scan


glm_data = '/data/d03/itwl/decoupler/compression/20150929T0000Z/umgl.pp8'
glm_mass = 'mass-cdi-glm-frozen-autosat.txt'

ukv_data = '/critical/fpos2/cylc-run/mi-af251/share/data/20151023T0900Z/ukv_um'
ukv_mass = 'mass-cdi-ukv-frozen-autosat.txt'
ukv_pb_stashes = [(1, 23, 0), (1, 24, 0), (1, 31, 0), (1, 33, 0),
                  (1, 3209, 0), (1, 3210, 0), (1, 16222, 0)]
ukv_pc_stashes = [(1, 10, 0), (1, 12, 0), (1, 254, 0), (1, 266, 0),
                  (1, 267, 0), (1, 268, 0), (1, 408, 0), (1, 409, 0),
                  (1, 9229, 0), (1, 16004, 0), (1, 16201, 0)]

glm_gpcs_data = '/data/d03/itwl/decoupler/data/global/20151019T1200Z/glm_um'
glm_gpcs_mass = 'mass-compression-sprint7-worst-case-glm.txt'
glm_gpcs_pa_stashes = [(1, 23, 0), (1, 24, 0), (1, 25, 0), (1, 26, 0),
                       (1, 30, 0), (1, 31, 0), (1, 33, 0), (1, 1201, 0),
                       (1, 1207, 0), (1, 1208, 0), (1, 1209, 0), (1, 1215, 0),
                       (1, 1216, 0), (1, 1235, 0), (1, 2201, 0),
                       (1, 2203, 128), (1, 2205, 0), (1, 2206, 0),
                       (1, 2207, 0), (1, 2422, 0), (1, 3026, 0),
                       (1, 3201, 128), (1, 3202, 128), (1, 3217, 128),
                       (1, 3223, 0), (1, 3224, 128), (1, 3225, 0),
                       (1, 3226, 0), (1, 3228, 128), (1, 3229, 128),
                       (1, 3231, 128), (1, 3232, 128), (1, 3234, 128),
                       (1, 3235, 128), (1, 3236, 0), (1, 3237, 0),
                       (1, 3241, 128), (1, 3245, 0), (1, 3248, 0),
                       (1, 3250, 0), (1, 3253, 0), (1, 3281, 0), (1, 3318, 0),
                       (1, 3319, 0), (1, 3321, 0), (1, 3401, 128),
                       (1, 3402, 128), (1, 3462, 0), (1, 3463, 0),
                       (1, 3463, 8192), (1, 4201, 128), (1, 4202, 128),
                       (1, 4203, 0), (1, 4204, 0), (1, 5201, 128),
                       (1, 5202, 128), (1, 5205, 128), (1, 5206, 128),
                       (1, 5207, 0), (1, 5208, 0), (1, 5210, 0), (1, 5211, 0),
                       (1, 5216, 0), (1, 5220, 0), (1, 5222, 0), (1, 5223, 0),
                       (1, 5224, 0), (1, 5225, 0), (1, 5226, 128), (1, 5262, 0),
                       (1, 6203, 0), (1, 8201, 128), (1, 8204, 128),
                       (1, 8205, 128), (1, 8208, 0), (1, 8209, 0), (1, 8223, 0),
                       (1, 8225, 0), (1, 9203, 0), (1, 9204, 0), (1, 9205, 0),
                       (1, 9210, 0), (1, 9212, 0), (1, 9216, 0), (1, 9217, 0),
                       (1, 9218, 0), (1, 9218, 128), (1, 9221, 0),
                       (1, 15201, 0), (1, 15202, 0), (1, 15212, 0),
                       (1, 15213, 0), (1, 15214, 0), (1, 15242, 0),
                       (1, 16202, 0), (1, 16203, 0), (1, 16205, 0),
                       (1, 16222, 0), (1, 16256, 0), (10, 40, 0), (10, 41, 0),
                       (10, 43, 0), (10, 44, 0), (10, 46, 0), (10, 47, 0),
                       (10, 48, 0), (10, 207, 0), (10, 220, 0),
                       (10, 20001, 1024), (10, 20002, 1024), (10, 20003, 0),
                       (10, 20004, 0), (10, 20005, 0), (10, 20006, 0),
                       (10, 20007, 0), (10, 20012, 0), (10, 20013, 128),
                       (10, 20014, 0), (10, 20015, 0), (10, 20016, 0),
                       (10, 20017, 0), (10, 20018, 0), (10, 20020, 0),
                       (10, 20021, 0), (10, 20022, 0), (10, 20023, 0),
                       (10, 20024, 0), (10, 20025, 0), (10, 20026, 0),
                       (10, 20027, 0), (10, 20028, 0), (10, 20029, 0),
                       (10, 20030, 0), (10, 20031, 0), (10, 20032, 0),
                       (10, 20033, 0), (10, 20034, 0), (10, 20035, 0),
                       (10, 20036, 0), (10, 20037, 0), (10, 20038, 0),
                       (10, 20041, 0), (10, 20042, 0), (10, 20043, 0),
                       (10, 20044, 0), (10, 20045, 0), (10, 20046, 0),
                       (10, 20047, 0), (10, 20048, 0), (10, 20049, 0),
                       (10, 20050, 0), (10, 20051, 0), (10, 20052, 0),
                       (10, 20053, 0), (10, 20054, 0), (10, 20055, 0),
                       (10, 20056, 0), (10, 20057, 0), (10, 20058, 0),
                       (10, 20059, 2048), (10, 20060, 0), (10, 20061, 0),
                       (10, 20062, 0), (10, 20063, 0), (10, 20065, 0),
                       (10, 20067, 0), (10, 20070, 0), (10, 20071, 0),
                       (10, 20236, 4096), (10, 20236, 8192)]
glm_gpcs_pb_stashes = [(1, 2, 0), (1, 3, 0), (1, 10, 0), (1, 12, 0),
                       (1, 12, 128), (1, 30, 0), (1, 33, 0), (1, 150, 0),
                       (1, 254, 0), (1, 254, 128), (1, 266, 0), (1, 407, 0),
                       (1, 408, 0), (1, 409, 0), (1, 1230, 0), (1, 1231, 0),
                       (1, 3219, 128), (1, 3220, 128), (1, 5212, 0),
                       (1, 6201, 0), (1, 6202, 0), (1, 6223, 0), (1, 6224, 0),
                       (1, 16004, 0), (1, 16201, 0)]

MB = 1024. * 1024.


def stringify(m, si, l):
    msg = '{:02d}_{:05d}_{:d}'
    return msg.format(m, si, l)


def msi(model, item):
    msg = 'm{:02d}s{:02d}i{:03d}'
    s = item / 1000
    i = item % 1000
    return msg.format(model, s, i)


def ratio(nc, ff):
    return nc / float(ff) if ff else -666


class Line(object):
    def __init__(self, fname, model, item, lbproc=0, nc=0, ff=0, fcount=0, chunk=None):
        self.fname = fname
        self.model = int(model)
        self.item = int(item)
        self.lbproc = int(lbproc)
        self.nc = int(nc)
        self.ff = int(ff)
        self.fcount = int(fcount)  # number of associated fields
        self.chunk = int(chunk) if chunk is not None else chunk  # time-step

    @property
    def ratio(self):
        return ratio(self.nc, self.ff)

    @property
    def stash(self):
        return (self.model, self.item, self.lbproc)

    @property
    def msi(self):
        return msi(self.model, self.item)

    def __repr__(self):
        msg = '<{s.fname}, model={s.model}, item={s.item}, ' \
            'lbproc={s.lbproc}, nc={s.nc}, ff={s.ff}, fcount={s.fcount}, ' \
            'chunk={s.chunk}, ratio={s.ratio:0.2f}'
        return msg.format(s=self)


def slurp(ifmass):
    with open(ifmass, 'r') as fi:
        lines = fi.readlines()
    result = {}
    for l in lines:
        l = l.strip()
        if l:
            l = l.split()
            nc, fname = l[4], l[8]
            fname = os.path.split(fname)[1]
            tmp = os.path.splitext(fname)[0].split('-')
            model, item, lbproc = tmp[-1].split('_')
            chunk = tmp[1] if len(tmp) == 4 else None
            line = Line(fname, model, item, lbproc=lbproc, nc=nc, chunk=chunk)
            if chunk is not None:
                result.setdefault(chunk, {})[line.stash] = line
            else:
                result[line.stash] = line
    return result


def ff_size(lines, ifdata, keys=None):
    sr = ff_scan(ifdata, lines)
    for k, v in sr.items():
        if keys is None or k in keys:
            lines[k].ff = v.total
            lines[k].fcount = v.fcount


def size(n_bytes):
    if n_bytes < 1024 * 1024 * 1024:
        return '{:.02f} MB'.format(n_bytes / 1024 / 1024.)
    elif n_bytes < 1024 * 1024 * 1024 * 1024:
        return '{:.02f} GB'.format(n_bytes / 1024 / 1024 / 1024.)
    else:
        return '{:.02f} TB'.format(n_bytes / 1024 / 1024 / 1024 / 1024.)


def worst_autosat_glm(table=True, wiki=False):
    lines = slurp(glm_mass)
    ff_size(lines, glm_data)
    ff, nc = 0, 0
    if table:
        if wiki:
            print '{| class="wikitable"'
            print '|-'
            print '! Stash !! lbproc !! NetCDF Bytes !! FF Bytes !! Ratio !! Field Count'
        else:
            print '|| Stash || lbproc || NetCDF Bytes || FF Bytes || Ratio || Field Count ||'
        for k in sorted(lines):
            l = lines[k]
            if wiki:
                msg = '| {} || {} || {} ({}) || {} ({}) || {:.2f} || {}'
                print '|-'
            else:
                msg = '| {} | {} | {} ({}) | {} ({}) | {:.2f} | {} |'
            print msg.format(l.msi, l.lbproc, l.nc, size(l.nc), l.ff, size(l.ff), l.ratio, l.fcount)
            nc += l.nc
            ff += l.ff
        if wiki:
            print '|}'
            print
            print '{| class="wikitable"'
            print '|-'
            print '! NetCDF Bytes Total !! FF Bytes Total !! Overall Ratio'
            print '|-'
            print '| {} ({}) || {} ({}) || {:.2f}'.format(nc, size(nc), ff, size(ff), ratio(nc, ff))
            print '|}'
        else:
            print '|| NetCDF Bytes Total || FF Bytes Total || Overall Ratio ||'
            print '| {} ({}) | {} ({}) | {:.2f} |'.format(nc, size(nc), ff, size(ff), ratio(nc, ff))
    return lines


def worst(mass, stream_stashes, data, table=True, all=False, wiki=False):
    chunks = slurp(mass)
    streams, stashes = zip(*stream_stashes)
    for stream, stash in zip(streams, stashes):
        for fname in glob(os.path.join(data, stream)):
            chunk = os.path.splitext(os.path.split(fname)[1].split('_')[-1])[0][-3::]
            lines = chunks[chunk]
            ff_size(lines, fname, keys=None)
    if table and not all:
        if wiki:
            print '{| class="wikitable"'
            print '|-'
            print '! Stash !! lbproc !! NetCDF Bytes !! FF Bytes !! Ratio !! Field Count'
        else:
            print '|| Stash || lbproc || NetCDF Bytes || FF Bytes || Ratio || Field Count ||'
    tff, tnc = 0, 0
    for stash in sorted(reduce(lambda x, y: x + y, stashes, [])):
        if table and all:
            if wiki:
                print '{| class="wikitable"'
                print '|-'
                print '! Stash !! Chunk !! lbproc !! NetCDF Bytes !! FF Bytes !! Ratio !! Field Count'
            else:
                print '|| Stash || Chunk || lbproc || NetCDF Bytes || FF Bytes || Ratio || Field Count ||'
        model, item, lbproc = stash
        fcount, ff, nc, lbproc = 0, 0, 0, set()
        for chunk in sorted(chunks):
            lines = chunks[chunk]
            if stash in lines:
                l = lines[stash]
                assert int(l.chunk) == int(chunk)
                fcount += l.fcount
                nc += l.nc
                ff += l.ff
                lbproc.add(l.lbproc)
                if table and all:
                    if wiki:
                        msg = '| {} || {} || {} || {} ({}) || {} ({}) || {:.2f} || {}'
                        print '|-'
                    else:
                        msg = '| {} | {} | {} | {} ({}) | {} ({}) | {:.2f} | {} |'
                    print msg.format(msi(model, item), l.chunk, l.lbproc, l.nc,
                                     size(l.nc), l.ff, size(l.ff),
                                     ratio(l.nc, l.ff), l.fcount)
        tff += ff
        tnc += nc
        if table and not all:
            lbproc = lbproc.pop() if len(lbproc) == 1 else tuple(sorted(lbproc))
            if wiki:
                msg = '| {} || {} || {} ({}) || {} ({}) || {:.2f} || {}'
                print '|-'
            else:
                msg = '| {} | {} | {} ({}) | {} ({}) | {:.2f} | {} |'
            print msg.format(msi(model, item), lbproc, nc, size(nc), ff, size(ff), ratio(nc, ff), fcount)
        else:
            print
    if table:
        if wiki:
            print '|}'
            print
            print '{| class="wikitable"'
            print '|-'
            print '! NetCDF Bytes Total !! FF Bytes Total !! Overall Ratio'
            print '|-'
            print '| {} ({}) || {} ({}) || {:.2f}'.format(tnc, size(tnc), tff, size(tff), ratio(tnc, tff))
            print '|}'
        else:
            print
            print '|| NetCDF Bytes Total || FF Bytes Total || Overall Ratio ||'
            print '| {} ({}) | {} ({}) | {:.2f} |'.format(tnc, size(tnc), tff, size(tff), ratio(tnc, tff))
    return chunks


def worst_autosat_ukv(table=True, all=False, wiki=False):
    streams = [('umqvaa_pb???', ukv_pb_stashes),
               ('umqvaa_pc???', ukv_pc_stashes)]
    return worst(ukv_mass, streams, ukv_data,
                 table=table, all=all, wiki=wiki)


def worst_gpcs_glm(table=True, all=False, wiki=False):
    streams = [('umglaa_pa???.gpcs', glm_gpcs_pa_stashes),
               ('umglaa_pb???.gpcs', glm_gpcs_pb_stashes)]
    return worst(glm_gpcs_mass, streams, glm_gpcs_data,
                 table=table, all=all, wiki=wiki)


class Record(object):
    def __init__(self, fcount, scount, ff, nc):
        self.fcount = fcount
        self.scount = scount
        self.ff = ff
        self.nc = nc

    def __repr__(self):
        msg = '<fcount={s.fcount}, scount={s.scount}, ff={s.ff}, nc={s.nc}>'
        return msg.format(s=self)


def plot(chunks, stashes, fontsize=10, rotation=45, direction='right',
         title='WTF', loc='upper left', ylim=None):
    from mpl_toolkits.axes_grid1 import host_subplot
    import mpl_toolkits.axisartist as AA
    from matplotlib.font_manager import FontProperties
    import matplotlib.pyplot as plt

    def _update(line, record):
        record.fcount += line.fcount
        record.scount += 1
        record.ff += line.ff
        record.nc += line.nc

    lookup = {}
    if stashes is not None:
        for stash in stashes:
            record = lookup.setdefault(stash, Record(0, 0, 0, 0))
            for chunk in sorted(chunks):
                lines = chunks[chunk]
                if stash in lines:
                    _update(lines[stash], record)
    else:
        for stash, line in chunks.items():
            record = lookup.setdefault(stash, Record(0, 0, 0, 0))
            _update(line, record)

    for k in sorted(lookup):
        print k, lookup[k]

    host = host_subplot(111, axes_class=AA.Axes)
    plt.subplots_adjust(right=0.75)

    par1 = host.twinx()
    par2 = host.twinx()

    offset = 80
    new_fixed_axis = par2.get_grid_helper().new_fixed_axis
    par2.axis["right"] = new_fixed_axis(loc="right",
                                        axes=par2,
                                        offset=(offset, 0))
    par2.axis["right"].toggle(all=True)

    x = range(len(lookup))

    host.set_xlim(x[0] - 1, x[-1] + 1)
    if ylim is not None:
        par1.set_ylim(*ylim)

    host.set_xlabel('Stash Codes')
    host.set_ylabel("Ratio (netCDF / FF)")
    par1.set_ylabel("Payload / MB")
    par2.set_ylabel("Field Count")

    host.set_xticks(x)
    host.set_xticklabels([stringify(*k) for k in sorted(lookup)])

    values = [lookup[k] for k in sorted(lookup)]

    ratio = [v.nc / v.ff for v in values]
    nc = np.array([v.nc for v in values]) / MB
    ff = np.array([v.ff for v in values]) / MB
    fcount = [v.fcount for v in values]

    p1, = host.plot(x, ratio, color='r', label="Ratio", marker='o')
    p2, = par1.plot(x, nc, color='g', label="netCDF", marker='s')
    p22, = par1.plot(x, ff, color='g', label="FF", marker='^', linestyle='--')
    p3, = par2.plot(x, fcount, color='b', label="Field Count", marker='*')

    font_properties = FontProperties(size='x-small')
    host.legend(loc=loc, fancybox=True, shadow=True, prop=font_properties)

    axis = host.axis["bottom"]
    axis.set_axis_direction(direction)
    axis.major_ticklabels.set_rotation(rotation)
    axis.major_ticklabels.set_fontsize(fontsize)
    host.axis["left"].label.set_color(p1.get_color())
    host.axis["left"].major_ticklabels.set_color(p1.get_color())
    par1.axis["right"].label.set_color(p2.get_color())
    par1.axis["right"].major_ticklabels.set_color(p2.get_color())
    par2.axis["right"].label.set_color(p3.get_color())
    par2.axis["right"].major_ticklabels.set_color(p3.get_color())

    plt.title(title)
    plt.draw()
    plt.show()


def plot_glm():
    # chunks = worst_gpcs_glm()  # on the cray
    import pickle
    with open('worst-case-glm.pkl', 'rb') as fi:
        chunks = pickle.load(fi)

    plot(chunks, glm_gpcs_pa_stashes, fontsize=6,
         rotation=-90, title="PA Global Frozen Worst-Case (GPCS to Raw & Uncompressed)")
    plot(chunks, glm_gpcs_pb_stashes, rotation=-45,
         title="PB Global Frozen Worst-Case (GPCS to Raw & Uncompressed)")


def plot_autosat_ukv():
    # chunks = worst_autosat_ukv()  # on the cray
    import pickle
    with open('worst-case-autosat-ukv.pkl', 'rb') as fi:
        chunks = pickle.load(fi)

    stashes = sorted(ukv_pb_stashes + ukv_pc_stashes)
    plot(chunks, stashes, rotation=-45,
         title='Autosat Frozen Worst-Case UKV (All time-chunks to Raw & Uncompressed)')


def plot_autosat_glm():
    # lines = worst_autosat_glm()  # on the cray
    import pickle
    with open('worst-case-autosat-glm.pkl', 'rb') as fi:
        lines = pickle.load(fi)
    plot(lines, None, rotation=-45, loc='upper right', ylim=(0, 10000),
         title='Autosat Frozen Worst-Case GLM (pp8 to Raw & Uncompressed)')
