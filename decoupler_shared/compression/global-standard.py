"""
Standard! data feed for GLOBAL model.
Regrid the data to get more strange numbers since the output of packed files
will be effectively precision-shaved.

"""
import os

from bgd.levels import HEIGHTS, PRESSURES
from bgd.plugins import *

import numpy as np

from biggus import ConstantArray
from iris.coord_systems import GeogCS
from iris.coords import DimCoord
from iris.cube import Cube
from iris.fileformats.pp import EARTH_RADIUS


def _make_global_grid(n_lat, min_lat, max_lat, n_lon, min_lon, max_lon,
                      bounds=True):
    """
    Create a two-dimensional Cube that represents the standard global grid.

    Returns
    -------
    Cube
        The standard global grid.

    """
    cs = GeogCS(EARTH_RADIUS)
    lon_coord = DimCoord(np.linspace(min_lon, max_lon, n_lon), 'longitude',
                         units='degrees', coord_system=cs)
    lat_coord = DimCoord(np.linspace(min_lat, max_lat, n_lat), 'latitude',
                         units='degrees', coord_system=cs)
    if bounds:
        lon_coord.guess_bounds()
        lat_coord.guess_bounds()
    cube = Cube(ConstantArray((n_lat, n_lon)))
    cube.add_dim_coord(lat_coord, 0)
    cube.add_dim_coord(lon_coord, 1)
    return cube


# The standardised global grid.
GLOBAL_GRID = _make_global_grid(1200, -89.925, 89.925,
                                1600, -179.8875, 179.8875)

TARGET_PATH = os.environ.get('GLM_STAGE_INPUT_DIR', '/data/d04/ithr/decoupler/global/2015051300')

TIME_CHUNK = int(os.environ.get('STAGE_TIME_CHUNK', '6'))

INPUT_PATHS = {
    'umglaa_pa': os.path.join(TARGET_PATH, 'umglaa_pa{:03d}.calc'.format(TIME_CHUNK)),
    'umglaa_pb': os.path.join(TARGET_PATH, 'umglaa_pb{:03d}'.format(TIME_CHUNK)),
}

OUTPUT_PATH = os.path.join(os.environ['DATADIR'], 'decoupler/global_standard/')

ATTRIBUTES = {'grid_id': 'glm_standard'}

LEVEL1 = {
    '01_00012_0': RegridBilinear(HybridHeightToTerrainFollowing(StructuredFFLoad('umglaa_pb', 'm01s00i012'), HEIGHTS), GLOBAL_GRID),
    '01_16004_0': RegridBilinear(HybridHeightToTerrainFollowing(StructuredFFLoad('umglaa_pb', 'm01s16i004'), HEIGHTS), GLOBAL_GRID),
    '01_16222_0': RegridBilinear(StructuredFFLoad('umglaa_pa', 'm01s16i222'), GLOBAL_GRID),
    '01_00254_0': RegridBilinear(HybridHeightToTerrainFollowing(StructuredFFLoad('umglaa_pb', 'm01s00i254'), HEIGHTS), GLOBAL_GRID),
    '01_00266_0': RegridBilinear(HybridHeightToTerrainFollowing(StructuredFFLoad('umglaa_pb', 'm01s00i266'), HEIGHTS), GLOBAL_GRID),
    '01_00002_0': RegridBilinear(HybridHeightToTerrainFollowing(StructuredFFLoad('umglaa_pb', 'm01s00i002'), HEIGHTS), GLOBAL_GRID),
    '01_03236_0': RegridBilinear(StructuredFFLoad('umglaa_pa', 'm01s03i236'), GLOBAL_GRID),
    '01_03281_0': RegridBilinear(StructuredFFLoad('umglaa_pa', 'm01s03i281'), GLOBAL_GRID),
    '01_00408_0': RegridBilinear(HybridHeightToTerrainFollowing(StructuredFFLoad('umglaa_pb', 'm01s00i408'), HEIGHTS), GLOBAL_GRID),
    '01_04203_0': RegridBilinear(StructuredFFLoad('umglaa_pa', 'm01s04i203'), GLOBAL_GRID),
}

# CDI has very specific requirements. We attach those to the processsed data.
LEVEL1 = {name: EnsureDimension(
                    SetAttributes(
                        ConvertTimeToSeconds(
                            ConvertModelLevelNumber(config)
                        ),
                        ATTRIBUTES
                    ),
                    'time'
                )
          for name, config in LEVEL1.items()}

LEVEL2 = {}

LEVEL3 = {}

