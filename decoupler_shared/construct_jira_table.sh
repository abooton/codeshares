# Script for constructing a JIRA table with timing information by searching through a job.out from a completed suite.

SUITE_PATH="/critical/fpos2/cylc-run/mi-al799_Develop-32-DD-499-Profile_ramdisk"

BATCH_NAME="glm_stage_frozen_pcs_short*"

RUN_NUMBER="03"

task_name=$(grep "Task ID" ${SUITE_PATH}/log/job/*/${BATCH_NAME}/${RUN_NUMBER}/job.out | cut -d "." -f 2 | cut -d ":" -f 3)
cycle_time=$(grep "Task ID" ${SUITE_PATH}/log/job/*/${BATCH_NAME}/${RUN_NUMBER}/job.out | cut -d "." -f 3)
queued_time=$(grep "Queued Time" ${SUITE_PATH}/log/job/*/${BATCH_NAME}/${RUN_NUMBER}/job.out | cut -d ":" -f 4,5)
elapsed_time=$(grep "Elapsed Time" ${SUITE_PATH}/log/job/*/${BATCH_NAME}/${RUN_NUMBER}/job.out | cut -d ":" -f 4,5)

export task_name=${task_name}
export cycle_time=${cycle_time}
export queued_time=${queued_time}
export elapsed_time=${elapsed_time}

python2.7 ./construct_jira_table.py
