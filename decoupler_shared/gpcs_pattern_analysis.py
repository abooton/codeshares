"""
Convert appearance patterns to group of "range" like expressions.

"""
import os
import os.path

import numpy as np


CONFIG = """\"\"\"
Global worst-case configuration.

\"\"\"
from bgd.plugins import *


OUTPUT_PATH = EnvironmentVar('GLM_STAGE_OUTPUT_DIR')

ATTRIBUTES = {{'grid_id': 'glm_worst'}}

mattrs = MonetAttributes('UKMO-1.0',                           # Conventions
                         'StaGE Decoupler',                    # history
                         'Met Office',                         # institution
                         'Met Office Unified Model',           # source
                         'Operational Global Model Forecast')  # title

PA_PATH = FFPath(EnvironmentVar('GLM_STAGE_INPUT_DIR'),
                 'umglaa_pa',
                 EnvironmentVar('STAGE_TIME_CHUNK'),
                 file_extension='.gpcs')
PB_PATH = FFPath(EnvironmentVar('GLM_STAGE_INPUT_DIR'),
                 'umglaa_pb',
                 EnvironmentVar('STAGE_TIME_CHUNK'),
                 file_extension='.gpcs')

# A dictionary mapping name --> plugin, for all the "Level 1.0" outputs.
LEVEL1 = {{
{}
}}

# CDI has very specific requirements. We attach those to the processsed data.
LEVEL1 = {{name: EnsureMonet(
                    EnsureDimension(
                        SetAttributes(
                            ConvertTimeToSeconds(
                                ConvertModelLevelNumber(config)
                            ),
                            ATTRIBUTES
                        ),
                        'time'
                    ),
                    mattrs
                )
          for name, config in LEVEL1.items()}}


# A dictionary mapping name --> plugin, for all the "Level 2.0" outputs.
LEVEL2 = {{}}


# A dictionary mapping name --> plugin, for all the "Level 3.0" outputs.
LEVEL3 = {{}}
"""


def gpcs_pattern_as_array(pattern_string, presence_char='x'):
    """
    Convert a string of 'x' and '.' to an array of 0s and 1s.

    E.G. 'xx.x.x...x' --> [1, 1, 0, 1, 0, 1, 0, 0, 0, 1]

    """
    return np.array([ch == presence_char for ch in pattern_string],
                    dtype=int)


# quick test
assert np.all(gpcs_pattern_as_array('xx.x.x...x') ==
              np.array([1, 1, 0, 1, 0, 1, 0, 0, 0, 1]))


# Set the nominal time interval between the gpcs timechunk files.
GPCS_BASE_INTERVAL = 3.0

def gpcs_pattern_as_timepoints(pattern_string):
    """
    Convert a gpcs pattern string to a set of occurrence-times.

    E.G. 'xx.x.x...x' --> [1, 1, 0, 1, 0, 1, 0, 0, 0, 1]
    E.G. 'x..x.x...x' --> [0., 3., 9., 15., 27.]

    """
    pattern_array = gpcs_pattern_as_array(pattern_string)
    occurence_indices = np.nonzero(pattern_array)[0]
    return np.array(GPCS_BASE_INTERVAL*occurence_indices, dtype=float)


# quick test
assert np.all(gpcs_pattern_as_timepoints('xx.x.x...x') ==
              np.array([0., 3., 9., 15., 27.]))


def identify_timestep_sections(times):
    """
    Detect regular repeats in a list of times (floats).

    Returns a list of (step-value, start-time, end-time).

    """
    if len(times) == 0:
        return []

    max_time = np.max(times) + GPCS_BASE_INTERVAL
    dts = np.diff(times)
    if len(dts) == 0:
        # Ensure we have a dummy 'zeroth' interval.
        dts = np.array([GPCS_BASE_INTERVAL])
    ddts = np.diff(dts)

    # Find 'section breaks' where the timestep changes.
    i_new_dt_starts = 1 + np.where(np.abs(ddts) > 0)[0]

    # Calculate the timesteps of sections.
    dt_section_start_inds = np.hstack(([0], i_new_dt_starts))
    section_dts = dts[dt_section_start_inds]

    # Calculate the section starts, at "0 + breaks".
    section_start_times = times[[0] + list(i_new_dt_starts + 1)]

    # Calculate the section ends, at "breaks + (end+1)".
    section_end_times = (section_start_times[1:]
                         - section_dts[1:] + GPCS_BASE_INTERVAL)
    section_end_times = np.hstack((section_end_times, [max_time]))

    return zip(section_dts, section_start_times, section_end_times)


def show_sections(timestep_sections):
    for i_sect, section in enumerate(timestep_sections):
        dt, t_start, t_end = section
        msg = '#{:02d}: from {:5.1f} to {:5.1f},  step={:5.1f} '.format(
            i_sect, t_start, t_end, dt)
        print msg
        msg = '  range({}, {}, {}) = {}'
        print msg.format(t_start, t_end, dt,
                         np.arange(t_start, t_end, dt))
    print


def test_pattern(pattern_string, expected_range_tuples):
    """Simply check that a pattern analyses as expected."""
    times = gpcs_pattern_as_timepoints(pattern_string)
    sections = identify_timestep_sections(times)
    msg = 'Pattern analysis fail:\n  pattern={}\n  Expected={}\n  Got={}'
    assert np.all(sections == expected_range_tuples), \
        msg.format(pattern_string, expected_range_tuples, sections)


def all_tests():
    test_pattern('', [])
    test_pattern('x', [(3., 0., 3.),])
    test_pattern('x..', [(3., 0., 3.),])
    test_pattern('.x', [(3., 3., 6.),])
    test_pattern('..x..', [(3., 6., 9.),])
    test_pattern('x.x..', [(6., 0., 9.),])
    test_pattern('xx', [(3., 0., 6.),])
    test_pattern('xx....', [(3., 0., 6.),])
    test_pattern('..xx....', [(3., 6., 12.),])
    test_pattern('xxxx.x.x', [(3., 0., 12.), (6., 15., 24.)])
    test_pattern('..xxxx.x.x', [(3., 6., 18.), (6., 21., 30.)])
    #             000011122233334
    #             036925814703692
    test_pattern('..xx.x.x.x.xxx',
                 [(3., 6., 12.),
                  (6., 15., 36.),
                  (3., 36., 42.)])
    #             0000111222333344455
    #             0369258147036925814
    test_pattern('..xxxx.x.x.x...xxx',
                 [(3., 6., 18.),
                  (6., 21., 36.),
                  (12., 45, 48.),  #NOTE: not ideal, but that's what it does!
                  (3., 48., 54.)])


def show_pattern_analysis(pattern):
    """Show how a pattern breaks down."""
    print 'Pattern:', pattern
    times = gpcs_pattern_as_timepoints(pattern)
    timestep_sections = identify_timestep_sections(times)
    show_sections(timestep_sections)


def stringify(codes):
    """Reformat the codes into LEVEL1 keys."""
    result = []
    for code in codes:
        m, si, l = code
        s = si / 1000
        i = si % 1000
        result.append('{:02d}_{:02d}{:03d}_{}'.format(m, s, i, l))
    return result


def stashify(codes):
    """Reformat the codes into (msi, lbproc)."""
    result = []
    for code in codes:
        m, si, l = code
        s = si / 1000
        i = si % 1000
        msi = 'm{:02d}s{:02d}i{:03d}'
        result.append((msi.format(m, s, i), l))
    return result


def codify(patterns):
    """Generate Rose suite stream patterns."""
    result = []
    for pattern, codes in patterns.items():
        print 'Pattern:', pattern
        times = gpcs_pattern_as_timepoints(pattern)
        timestep_sections = identify_timestep_sections(times)
        show_sections(timestep_sections)

        ranges = [(start, stop, step) for (step, start, stop) in timestep_sections]
        scodes = stringify(sorted(codes))

        for sss in ranges:
            result.append((map(int, sss), scodes))
    return result


def generate_config(fname='config_global_worst.py'):
    pa_patterns = load_pa()
    pb_patterns = load_pb()
    _codes = lambda x: sorted(reduce(set.__or__, x.values(), set()))
    pa_codes = _codes(pa_patterns)
    pb_codes = _codes(pb_patterns)
    lines = []
    for stream, codes in [('PA_PATH', pa_codes),
                          ('PB_PATH', pb_codes)]:
        tasks = zip(stringify(codes), stashify(codes))
        for task in tasks:
            key, (msi, lbproc) = task
            if lbproc:
                msg = '    {!r}: StructuredFFLoad({!r}, {!r}, lbproc={}),'
            else:
                msg = '    {!r}: StructuredFFLoad({!r}, {!r}),'
            lines.append(msg.format(key, stream, msi, lbproc))
    lines = '\n'.join(lines)
    with open(fname, 'w') as fo:
        fo.write(CONFIG.format(lines))


def load(fname):
    import pickle
    with open(fname, 'r') as fi:
        patterns = pickle.load(fi)
    return patterns

_THIS_DIR = os.path.dirname(__file__)

def load_pa():
    return load(os.path.join(_THIS_DIR, 'pa_patterns_and_codes.pkl'))


def load_pb():
    return load(os.path.join(_THIS_DIR, 'pb_patterns_and_codes.pkl'))


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', help='Run config generation',
                        type=str)
    parser.add_argument('-p', '--patterns',
                        help='Show pattern analysis for one or more comma-separated pkl files',
                        type=lambda x: x.split(','))
    parser.add_argument('-t', '--test', help='Run tests',
                        action='store_const', const=True)
    args = parser.parse_args()

    if args.config:
        generate_config(args.config)
    if args.patterns:
        for testfile_path in args.patterns:
            test_patterns = load(testfile_path)
            for pattern, _codes in test_patterns.items():
                show_pattern_analysis(pattern)
    if args.test:
        all_tests()

