#PBS -N decoupler_test_data_retrieval
#PBS -j oe
#PBS -l walltime=03:00:00
#PBS -l select=1:ncpus=32
#PBS -q shared

# A script for extracting the latest version of the test data
# from MASS 
# The models that will be extracted must be added to the 
# models string.
# The files that will be retrieved must match pattern match
# with wildcards the filenames specified in files_for_retrieval
models="ukv glm engl"
files_for_retrieval="readme umgl.pp8 ????aa_p*"

# Function to take the file path after the test_data directory.
function suffix_finder {
python2.7 <<END
import os 
print os.environ['filepath'].split("test_data")[1]
END
}

# Function to identify whether the file in MASS matches
# the wildcarded pattern in files_for_retrieval.
# This ensures files are not extracted for MASS multiple
# times, and prevents whole directories being extracted,
# which do not match the pattern.
function suffix_checker {
python2.7 <<END
import fnmatch
import os
files_for_retrieval=os.environ["files_for_retrieval"]
for afile in files_for_retrieval.split(" "):
    if fnmatch.fnmatch(os.path.basename(os.environ['suffix']), afile):
        print True
        break
else:
    print False
END
}

test_data_dir="${DATADIR}/decoupler/test_data"
mkdir -p ${test_data_dir}
export files_for_retrieval=$files_for_retrieval

# Loop through models, finding the most recently created 
# DATA_VERSION for each model. 
for model in $models; do
    model_mass_dir=$(moo ls -t moose:/adhoc/projects/hpcdecoupler/decoupler/test_data/${model} | tail -n 1)
    files=$(moo ls -r "${model_mass_dir}")
    # Loop over all files within the most recently created
    # DATA_VERSION for the particular model.
    for f in $files;do        
        export filepath=$f
        suffix=$(suffix_finder)
        export suffix=$suffix
        suffix_logical=$(suffix_checker)
        # If the suffix matches the wildcarded patterns
        # within files_for_retrieval.
        if [ $suffix_logical == True ]; then
            getfname=${test_data_dir}/${suffix}
            getdir=$(dirname $getfname)
            mkdir -p ${getdir}
            # If the file or directory does not already exist,
            # get from MASS, otherwise skip retrieval.
            if [ ! -f $getfname -a ! -d $getfname ]; then
                moo get -f ${f} ${getdir}
            else
                echo "Skipping: $getfname already exists"
            fi
            [ -L  ${getfname} ] && rm ${getfname}
        else
            echo "Not retrieving ${f} with a suffix matching ${suffix}"
        fi
    done
done
echo "Extracted files:"
ls -LR ${test_data_dir}/*
