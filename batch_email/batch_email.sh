#/bin/bash

#------------------------------------------------------------------------------
# PURPOSE
#
# Send an email (BCC) with a defined subject and body (as HTML) to a list of
# emailes that are contained in a file.  This file should be a single line
# with each recipients email address seperate by a semi colon.

# At this time this script does *not* handle multiple files with the BCC
# recipient list.
#
# The default REPLYTO email will be AVD Support.  This script should be run
# as the avd user.
#
#
# !!!!BE CAREFUL!!!!
#
# Test this script using a your own email adddress until you are satisfied
# the contents is correct.  You can do this by creating a file with your email
# address on the first line and then point EMAIL_BCC_FILE to it.
# 
# The only values to change in this script should be:
#
# EMAIL_BCC_FILE  : This is the file holding a semi colon list of users to email
# EMAIL_SUBJECT   : The email subject line
# EMAIL_BODY      : The body text (HTML) to include in the email
#------------------------------------------------------------------------------

export REPLYTO="ml-avd-support@metoffice.gov.uk"

#------------------------------------------------------------------------------
# Configuration section for the email subject, body and BCC recipiants.
#------------------------------------------------------------------------------

EMAIL_BCC_FILE=/project/avd/python/usage_logs/env_users/email_test.txt
EMAIL_SUBJECT="Scientific Software Stack Notification: Environment to be Decommissioned"
EMAIL_BODY="$(cat << EOF
<h2>Why am I receiving this email?</h2>
<p>
You have been identified as an active user over the last 
<b>90 days</b> of the enviromment:

<ul>
<li><font color="red"><b>scitools/default_legacy-next</li></b></font>
</ul>

This environment will be decomissioned from <font color="red"><b>03/02/2020</b></font>.  
For more information on planned new environments or other environment decommissioning 
plans pleasee see the 
<a href="http://www-avd/sci/software_stack/roadmap.html">roadmap</a>.
</p>

<h2>What do I need to do?</h2>
<p>
If you are actively using this environment please consider using a more 
recent alternative.  For a complete list of the Scientific Software Stack
Environments available please the 
<a href="http://www-avd/sci/software_stack/environments_available.html">Environment Browser</a> 
in the <a href="http://www-avd/sci/software_stack">documentation</a>.
</p>


<h2>How do you know I used this environment?</h2>
<p>
Use of the Scientific Software Stack is logged.  For more information see the
<a href="http://www-avd/sci/software_stack/usage_logs.html">usage logs</a>.
You can view the logs directly 
<a href="http://www-avd/nbviewer/MetOffice/file_/project/avd/python/usage_logs/notebook/SSS_env_usage.ipynb#shortcuts_to_env_listings">here</a>. 
</p>


<h2>Do you need further help?</h2>
<p>
Please contact the
<a href="https://metoffice.sharepoint.com/sites/TechSolDelAVDTeam/SitePages/AVD-Support.aspx?web=1">AVD Team</a>.
</p>

<p>
Regards,
</p>

<p>
<b>The AVD Team</b></br>
<a href="https://metoffice.sharepoint.com/sites/TechSolDelAVDTeam/SitePages/AVD-Surgery-Main.aspx?web=1">AVD Surgeries</a> | 
<a href="https://metoffice.sharepoint.com/sites/TechSolDelAVDTeam/SitePages/AVD-Support.aspx?web=1">AVD Support</a> | 
<a href="http://www-avd/sci/software_stack/">Scientific Software Stack Documentation</a>
</p>
EOF
)"

#------------------------------------------------------------------------------
# Check the BCC email file exists
#------------------------------------------------------------------------------

if [[ ! -f ${EMAIL_BCC_FILE} ]]
then
	echo "ERROR: EMAIL_BCC_FILE does not exist, ${EMAIL_BCC_FILE}"
	echo "Aborting."
	exit 1
fi

#------------------------------------------------------------------------------
# Check the user wants to send the email
#------------------------------------------------------------------------------

# assume a single line with all the email one, semi colon seperated
read -r EMAIL_BCC_LIST < ${EMAIL_BCC_FILE}

echo "================================================================"
echo "WARNING, proceeding will send an email to the following users:  "
echo "================================================================"
echo
echo "${EMAIL_BCC_LIST}"
echo
echo "This email list was sourced from: "
echo "    ${EMAIL_BCC_FILE}"
echo

read -r -p "Are you sure? [y/n] " response

response=${response,,}   

if [[ ! "$response" =~ ^(yes|y)$ ]] 
then
	echo "Aborting."
	exit 1
fi

#------------------------------------------------------------------------------
# Send the email
#------------------------------------------------------------------------------

echo
echo "Sending email..."
echo ${EMAIL_BODY} | mutt -e "set content_type=text/html" -s "${EMAIL_SUBJECT}" -b ${EMAIL_BCC_LIST}
echo "Return Code = $?"


